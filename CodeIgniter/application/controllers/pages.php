<?php
//controlador paginas
class Pages extends CI_Controller{
	
	public function view($page='home'){
		if(!file_exists('application/views/pages/'.$page.'.php'))
		{
			
			  //whoops, we dont have a page for that!
			  show_404();
		}
		
		$data['title']=ucfirst($page); //capitalizar la primera letra
		
		$this->load->view('templates/header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
} 

?>